using System;

namespace state_pattern
{
    public class Context
    {
        private AbstractTransportation currentState = new Car();

        public void setState(AbstractTransportation newState)
        {
            currentState = newState;
        }
        public void printName()
        {
            currentState.reportState();
        }
        public double speed()
        {
            return currentState.speed;
        }
        public void report()
        {
            this.printName();
            Console.WriteLine("With the speed of: " + this.speed() + " m/s.");
        }
        public void attack(){
            currentState.attack();
        }
    }
}
