using System;

namespace state_pattern
{
    public class Vehicle
    {
        public enum Mode { Car, Ship, Airplane, UndergroundTrain, Submarine, SpaceShip };
        public Mode mode = Mode.Car;
        public double speed = 14;
        public void setMode(Mode _mode)
        {
            mode = _mode;
            switch (mode)
            {
                case Mode.Car: speed = 14; break;
                case Mode.Ship: speed = 10; break;
                case Mode.Airplane: speed = 72; break;
                case Mode.UndergroundTrain: speed = 12; break;
                case Mode.Submarine: speed = 18; break;
                case Mode.SpaceShip: speed = 299999999; break;
            }
        }
        public void reportState()
        {
            switch (mode)
            {
                case Mode.Car:
                    Console.WriteLine("You are driving a car."); break;
                case Mode.Ship:
                    Console.WriteLine("You are enjoying fresh breeze of the sea."); break;
                case Mode.Airplane:
                    Console.WriteLine("You are on an airplane."); break;
                case Mode.UndergroundTrain:
                    Console.WriteLine("You are on an underground train."); break;
                case Mode.Submarine:
                    Console.WriteLine("You are under the deep sea."); break;
                case Mode.SpaceShip:
                    Console.WriteLine("You are travelling in the space."); break;
            }
        }
    }
}
