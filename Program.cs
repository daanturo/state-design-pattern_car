﻿using System;
namespace state_pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            bool water = false;
            int height = 0;
            Context context = new Context();
            context.report();
            while (true)
            {
                Console.WriteLine("Type 'w' to move to watery area, 'g' to move to a ground area.");
                Console.WriteLine("Type 'a' to attack.");
                Console.WriteLine("Or type a number to specify height in kilometer.");
                Console.WriteLine("Type q to exit");
                string input = Console.ReadLine();
                if (input == "w") { water = true; }
                else if (input == "g") { water = false; }
                else if (input == "q") { break; }
                else if (input == "a") { context.attack(); }
                else
                {
                    int newheight = 0;
                    bool success = Int32.TryParse(input, out newheight);
                    if (success) height = newheight;
                }
                if (height > 11) context.setState(new SpaceShip());
                else if (height == 0)
                {
                    if (water == true) context.setState(new Ship());
                    else context.setState(new Car());
                }
                else if (height < 0)
                {
                    if (water == true) context.setState(new Submarine());
                    else context.setState(new UndergroundTrain());
                }
                else context.setState(new Airplane());
                Console.WriteLine();
                context.report();
                Console.WriteLine();
            }
        }
    }
}
