using System;

namespace state_pattern
{
    public abstract class AbstractTransportation
    {
        public abstract void reportState();
        public double speed = 0.5;
        public abstract void attack();
    }
    public class Car : AbstractTransportation
    {
        public Car() { speed = 14; }
        public override void reportState()
        {
            Console.WriteLine("You are driving a car.");
        }
        public override void attack()
        {
            Console.WriteLine("Shoot a performance of firework.");
        }
    }
    public class Ship : AbstractTransportation
    {
        public Ship() { speed = 10; }
        public override void reportState()
        {
            Console.WriteLine("You are enjoying fresh breeze of the sea.");
        }
        public override void attack()
        {
            Console.WriteLine("Shoot water");
        }
    }
    public class Airplane : AbstractTransportation
    {
        public Airplane() { speed = 72; }
        public override void reportState()
        {
            Console.WriteLine("You are on an airplane.");
        }
        public override void attack()
        {
            Console.WriteLine("Useless");
        }
    }
    public class UndergroundTrain : AbstractTransportation
    {
        public UndergroundTrain() { speed = 12; }
        public override void reportState()
        {
            Console.WriteLine("You are on an underground train.");
        }
        public override void attack()
        {
            Console.WriteLine("Useless");
        }
    }
    public class Submarine : AbstractTransportation
    {
        public Submarine() { speed = 18; }
        public override void reportState()
        {
            Console.WriteLine("You are under the deep sea.");
        }
        private void load_torpedo() { }
        public override void attack()
        {
            this.load_torpedo();
            Console.WriteLine("Torpedo loaded, shooting.");
        }
    }
    public class SpaceShip : AbstractTransportation
    {
        public SpaceShip() { speed = 299999999; }
        public override void reportState()
        {
            Console.WriteLine("You are travelling in the space.");
        }
        private int backup_small_ships = 8;
        private void prepare_bullets() { }
        private void prepare_sub_ships() { backup_small_ships--; }
        public override void attack()
        {
            prepare_bullets();
            prepare_sub_ships();
            Console.WriteLine("Shooting bullets.");
            if (backup_small_ships > -1) Console.WriteLine("Deploying small ships.");
        }
    }
}
